const Order = require("./Assignment1_Order");

const OrderState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    TYPE:   Symbol("type"),
    BREAD_TYPE:   Symbol("bread type"),
    D_TYPE:   Symbol("donut type"),
    D_SIZE:   Symbol("donut size"),
    MUFFINS:  Symbol("muffins")
});

module.exports = class SandwichOrder extends Order{
    constructor(){
        super();
        this.stateCur = OrderState.WELCOMING;
        this.sType = "";
        this.sBreadType = "";
        this.sMuffins = "";
        this.sItem = "sandwich";
        this.sDountType = '';
        this.sDountSize = '';
        this.sPrice = 0;
    }
    handleInput(sInput){
        let aReturn = [];
        switch(this.stateCur){
            case OrderState.WELCOMING:
                this.stateCur = OrderState.TYPE;
                aReturn.push("Welcome to Tim horton's.");
                aReturn.push("What Type of sandwich would you like?");
                break;
            case OrderState.TYPE:
                this.stateCur = OrderState.BREAD_TYPE
                this.sType = sInput;
                if(this.sType.toLowerCase() == 'bacon') this.sPrice+=4;
                else if(this.sType.toLowerCase() == 'blt') this.sPrice+=4.5;
                else if(this.sType.toLowerCase() == 'garden') this.sPrice+=5;
                else this.sPrice+=3.5;
                
                aReturn.push("What type of bread would you like?");
                break;
            case OrderState.BREAD_TYPE:
                this.stateCur = OrderState.D_TYPE
                this.sBreadType = sInput;
                if(this.sBreadType.toLowerCase()=='classic') this.sPrice+=1;
                else if(this.sBreadType.toLowerCase()=='bagel') this.sPrice+=1.5;
                else this.sPrice+=0.5;
                aReturn.push("What Type of Donut would you like?");
                break;
            case OrderState.D_TYPE: //second item - donut
                this.stateCur = OrderState.D_SIZE;
                this.sDountType=sInput;
                if(this.sDountType.toLowerCase()=='chocolate') this.sPrice+=2;
                else if(this.sDountType.toLowerCase()=='vanilla') this.sPrice+=1.5;
                else if(this.sDountType.toLowerCase()=='honey') this.sPrice+=1;
                else this.sPrice+=1;
                aReturn.push("what size of donut would you like?");
                break;
            case OrderState.D_SIZE:
                this.stateCur = OrderState.MUFFINS;
                this.sDountSize=sInput;
                if(this.sDountType.toLowerCase()=='small') this.sPrice+=1;
                else if(this.sDountType.toLowerCase()=='medium') this.sPrice+=1.5;
                else if(this.sDountType.toLowerCase()=='large') this.sPrice+=2;
                else this.sPrice+=1.5;
                aReturn.push("Would you like Muffins with that?");
                break;
            case OrderState.MUFFINS://up-sell
                this.isDone(true);
                if(sInput.toLowerCase() != "no"){
                    this.sMuffins = sInput;
                    this.sPrice+=1.5;
                }
                aReturn.push("Thank-you for your order of");
                aReturn.push(`${this.sType} ${this.sItem} with ${this.sBreadType} Bread`);
                aReturn.push(`${this.sDountType} Donut with ${this.sDountSize} size`);
                if(this.sMuffins){
                    aReturn.push(this.sMuffins);
                }
                aReturn.push(`Total: $${this.sPrice}`);
                let d = new Date(); 
                d.setMinutes(d.getMinutes() + 20);
                aReturn.push(`Please pick it up at ${d.toTimeString()}`);
                break;
        }
        return aReturn;
    }
}